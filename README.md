# Hermes

## Install

In the root directory:

```bash
pip install -e .
cd hermes/web
npm install
npm run build
```

## Run experiments

The command for running experiments is a thin wrapper over mrunner. The syntax is exactly the same:

```bash
hermes --context eagle run my_awesome_spec.py
```

## View experiments

For now we only have the web UI:

```bash
hermes web
```
