from setuptools import find_packages
from setuptools import setup


setup(
    name='hermes',
    description='Tool for managing experiments.',
    version='0.0.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'flask',
        'paramiko',
        'pyyaml',
    ],
    entry_points={
        'console_scripts': [
            'hermes=hermes.cli:main'
        ],
    },
)
