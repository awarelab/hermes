import os

import yaml

from hermes import data
from hermes import os as hermes_os


def _db_path(filename):
    base_path = os.path.join(os.path.expanduser('~'), '.hermes')
    os.makedirs(base_path, exist_ok=True)
    db_path = os.path.join(base_path, filename)
    if not os.path.exists(db_path):
        # Touch the db file.
        open(db_path, 'w').close()
    return db_path


def _sweep_db_path():
    return _db_path("sweeps.yaml")


def _experiment_db_path():
    return _db_path("experiments.yaml")


def _load(path, data_type):
    with open(path, 'r') as f:
        data_dict = yaml.load(f, Loader=yaml.Loader) or {}
    return {
        name: data_type(**content)
        for (name, content) in data_dict.items()
    }


def load_sweeps():
    return _load(_sweep_db_path(), data.Sweep)


def load_experiments():
    return _load(_experiment_db_path(), data.Experiment)


def _save(path, data_dict):
    data_dict = {
        name: content._asdict()
        for (name, content) in data_dict.items()
    }
    with open(path, 'w') as f:
        yaml.dump(data_dict, f)


def _save_sweeps(sweep_dict):
    with hermes_os.atomic_dump([_sweep_db_path()]) as [path]:
        _save(path, sweep_dict)


def _save_experiments(experiment_dict):
    with hermes_os.atomic_dump([_experiment_db_path()]) as [path]:
        _save(path, experiment_dict)


def _save_sweeps_and_experiments(sweep_dict, experiment_dict):
    with hermes_os.atomic_dump([_sweep_db_path(), _experiment_db_path()]) as [
        sweep_path, exp_path
    ]:
        _save(sweep_path, sweep_dict)
        _save(exp_path, experiment_dict)


def insert_sweep(sweep, experiments):
    sweep_dict = load_sweeps()
    exp_dict = load_experiments()
    assert sweep.name not in sweep_dict
    sweep_dict[sweep.name] = sweep
    for exp in experiments:
        assert exp.name not in exp_dict
        exp_dict[exp.name] = exp

    _save_sweeps_and_experiments(sweep_dict, exp_dict)


def update_experiment(experiment):
    experiment_dict = load_sweeps()
    assert exp.name in experiment_dict
    experiment_dict[exp.name] = exp
    _save_experiments(experiment_dict)
