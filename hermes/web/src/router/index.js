import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Experiments from '../views/Experiments.vue'
import ExperimentPlots from '../views/ExperimentPlots.vue'
import ExperimentLogs from '../views/ExperimentLogs.vue'
import ExperimentProperties from '../views/ExperimentProperties.vue'

Vue.use(VueRouter)
Vue.use(VueAxios, axios)

const experimentRoute = (slug, text, component) => ({
    path: '/exp/:id/' + slug,
    name: 'experiment_' + slug,
    component,
    meta: {
      breadcrumb: [
        { text: 'Experiments', to: 'experiments' },
        { textFromRoute: (route) => route.params.id, to: 'experiment' },
        { text },
      ],
      experiment: true,
    },
});

const routes = [
  {
    path: '/',
    name: 'experiments',
    component: Experiments,
    meta: {
      breadcrumb: [
          { text: 'Experiments' }
      ],
    },
  },
  {
    path: '/exp/:id',
    name: 'experiment',
    redirect: { name: 'experiment_plots' },
  },
  experimentRoute('plots', 'Plots', ExperimentPlots),
  experimentRoute('logs', 'Logs', ExperimentLogs),
  experimentRoute('properties', 'Properties', ExperimentProperties),
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
