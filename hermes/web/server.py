import flask

from hermes import backend


app = flask.Flask(__name__, static_url_path='', static_folder='dist')


@app.route('/')
def index():
    return app.send_static_file('index.html')


@app.route('/css/<path:path>')
def css(path):
    return flask.send_from_directory('dist/css', path)


@app.route('/js/<path:path>')
def js(path):
    return flask.send_from_directory('dist/js', path)


@app.route('/api/experiments')
def experiments():
    exps = backend.experiments()
    return flask.jsonify([
        exp._replace(status=exp.status.name)._asdict()
        for exp in exps
    ])


@app.route('/api/sweeps')
def sweeps():
    exps = backend.sweeps()
    return flask.jsonify([exp._asdict() for exp in exps])


if __name__ == '__main__':
    app.run()
