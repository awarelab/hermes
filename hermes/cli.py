import argparse
import sys

from mrunner.cli import mrunner_cli

from hermes import backend
from hermes.web import server as web_server


def main():
    # Forward args for the commands "run" and "context" without parsing
    # - they're going to be parsed by mrunner.
    if 'run' in sys.argv:
        backend.mrunner_run()
        return
    elif 'context' in sys.argv:
        backend.mrunner_context()
        return

    # The rest of the commands are handled by Hermes.
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    parser_web = subparsers.add_parser('web')
    parser_web.set_defaults(func=web_server.app.run)

    args = vars(parser.parse_args())
    func = args.pop('func')
    func(**args)


if __name__ == '__main__':
    main()
