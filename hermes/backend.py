import datetime
import re
import os
import sys

from mrunner.cli import mrunner_cli
from paramiko import SSHClient

from hermes import data
from hermes import db


def build_sweep(mrunner_exps):
    pass


def call_mrunner():
    sys.argv[0] = 'mrunner'
    mrunner_cli.cli()


def convert_mrunner_sweep(mrunner_sweep, mrunner_experiments):
    scratch_dir = mrunner_sweep.experiment_scratch_dir

    # Warning: correctnes of the paths constructed below depends on the
    # script template (slurm_experiment.sh.jinja2)
    path, partial_sweep_name = os.path.split(scratch_dir)
    unique_sweep_name = os.path.basename(path)

    experiments = [
        data.Experiment(
            name=f"{unique_sweep_name}/{partial_sweep_name}_{array_id}",
            tags=mrunner_experiment['tags'],
            sweep_name=unique_sweep_name,
            working_dir=f"{scratch_dir}_{array_id}",
            log_path=f"{scratch_dir}/slurm_{array_id}.log",
            status = None,
            started_at = datetime.datetime.now(),
            job_id = None,
            url = None,
        )
        for (array_id, mrunner_experiment) in enumerate(mrunner_experiments)
    ]

    example_experiment = mrunner_experiments[0]
    neptune_org, neptune_project = mrunner_sweep.project.split('/')
    sweep = data.Sweep(
        name=unique_sweep_name,
        note=None,
        due_date=None,
        neptune_org=neptune_org,
        neptune_project_name=neptune_project,
        neptune_tags=example_experiment['tags'],
        group_by=None,
        to_do=None,
        description=mrunner_sweep.name,
        cluster=mrunner_sweep.slurm_url,
        created_at=datetime.datetime.now(),
        predecessors=[],
        style='standard'
    )
    return (sweep, experiments)


def mrunner_run():
    def after_run(sweep, experiments):
        (sweep, experiments) = convert_mrunner_sweep(sweep, experiments)
        db.insert_sweep(sweep, experiments)

    mrunner_cli.register_after_run_callback(after_run)
    call_mrunner()


def mrunner_context():
    call_mrunner()


def experiments(
    # Filters - str; wildcards, should match fields in data.Experiment:
    experiment_name='.*',
    sweep_name='.*',
    cluster='.*',
    # Filter - [str] (Experiment.tags should contain this subset of tags):
    tags=None,
    # Ordering of the results.
    # Syntax: {+|-}{name|job_id|status|sweep_name|started_at}.
    order_by='+name',
):
    """Returns a list of Experiments."""
    # TODO regex?
    assert order_by[0] in "-+" and order_by[1:] in ["name", "job_id", "status", "sweep_name", "started_at"]
    re.compile(experiment_name)
    re.compile(sweep_name)
    re.compile(cluster)

    experiment_list = list(db.load_experiments().values())
    sweep_dict = db.load_sweeps()

    def filter_exp(exp):
        # TODO change regex to wildcards?
        match = True
        match &= bool(re.match(experiment_name, exp.name))
        match &= bool(re.match(sweep_name, exp.sweep_name))
        assert exp.sweep_name in sweep_dict, "database is not consistent"
        match &= bool(re.match(cluster, sweep_dict[exp.sweep_name].cluster))
        if tags:
            for tag in tags:
                match &= tag in exp.tags

        return match

    filtered_experiments = list(filter(filter_exp, experiment_list))
    sorted_experiments = sorted(filtered_experiments, key=lambda x: getattr(x, order_by[1:]), reverse="-" in order_by)

    return sorted_experiments


def log(
    experiment_name,  # str; should match Experiment.name exactly
):
    """Returns the slurm.log of a given Experiment."""

    experiment_dict = db.load_experiments()
    sweep_dict = db.load_sweeps()

    assert experiment_name in experiment_dict, "wrong experiment name"
    exp = experiment_dict[experiment_name]
    cluster = sweep_dict[exp.sweep_name].cluster
    user, address = cluster.split("@")

    ssh = SSHClient()
    ssh.load_system_host_keys()
    ssh.connect(address, username=user)
    _, ssh_stdout, _ = ssh.exec_command(f'cat {exp.log_path}')
    return ssh_stdout.read(-1)


def kill(
    experiment_names,  # [str]; names of experiments to kill
):
    """Kills a list of experiments."""

    experiment_list = list(db.load_experiments().values())
    sweep_dict = db.load_sweeps()

    def filter_exp(exp):
        return exp.name in experiment_names

    filtered_experiments = filter(filter_exp, experiment_list)

    # TODO Batch it
    for exp in filtered_experiments:

        cluster = sweep_dict[exp.sweep_name].cluster
        user, address = cluster.split("@")
        ssh = SSHClient()
        ssh.load_system_host_keys()
        ssh.connect(address, username=user)
        _, ssh_stdout, _ = ssh.exec_command(f'scancel {exp.job_id}')

        # TODO add active waiting for abortion
        exp.id = data.Status.aborted
        db.update_experiment(exp)


def sweeps(
    # Same as in experiments():
    sweep_name='.*',
    cluster='.*',
    # TODO tags? there are not tags for sweeps yet
    # tags=None,
    # Ordering of the results.
    # Syntax: {+|-}{name|description|cluster}.
    order_by='+name',
):
    """Returns a list of Sweeps."""
    # TODO regex?
    assert order_by[0] in "-+" and order_by[1:] in ["name", "job_id", "status", "sweep_name", "started_at"]
    re.compile(sweep_name)
    re.compile(cluster)

    sweep_list = list(db.load_sweeps().values())

    def filter_sweeps(sweep):
        # TODO change regex to wildcards?
        match = True
        match &= bool(re.match(sweep_name, sweep.name))
        match &= bool(re.match(cluster, sweep.cluster))
        return match

    filtered_sweeps = list(filter(filter_sweeps, sweep_list))
    sorted_sweeps = sorted(filtered_sweeps, key=lambda x: getattr(x, order_by[1:]), reverse="-" in order_by)

    return sorted_sweeps


def graph(
    sweep_names,  # [str]; names of sweeps to show in the graph
):
    """Returns Mermaid code for rendering a sweep graph."""
    return """
    graph LR
        14838850597713616900["Obverter setup of PK<br> <br><small>Reproduction PK results</small>"]
        class 14838850597713616900 description;
        click 14838850597713616900 "https://ui.neptune.ai/o/pmtest/org/noise-channel/experiments?viewId=standard-view&sortBy=%5B%22timeOfCreation%22%5D&sortDirection=%5B%22descending%22%5D&sortFieldType=%5B%22native%22%5D&trashed=false&tags=%5B%5D&lbViewUnpacked=true"

        12280474376466531265["Obverter base experiment<br>experiment_2001, condescending_pare <br><small>Similar results as before.</small>"]
        14838850597713616900-->|Reproduction|12280474376466531265
        class 12280474376466531265 done;
        click 12280474376466531265 "https://ui.neptune.ai/o/pmtest/org/noise-channel/experiments?viewId=standard-view&sortBy=%5B%22timeOfCreation%22%5D&sortDirection=%5B%22descending%22%5D&sortFieldType=%5B%22native%22%5D&sortFieldAggregationMode=%5B%22auto%22%5D&trashed=false&suggestionsEnabled=true&tags=%5B%22condescending_pare%22%5D&lbViewUnpacked=true&groupBy=%5B%22noisy_channel.noise%22%5D&groupByFieldType=%5B%22numericParameters%22%5D"

    classDef standard fill:#FFDC00;
    classDef done fill:#39CCCC;
    classDef toReview fill:#FF851B;
    classDef metaMissing fill:#f77;
    classDef todo fill:#7FDBFF;
    classDef decription fill:#ffff00
    """
