import collections
import datetime
import enum


class Status(enum.Enum):

    # Lifecycle:
    # scheduling -> pending -> running -> finished
    #                                  -> aborting -> aborted
    #                                  -> error

    # Semantics:
    # scheduling: user executed "hermes run", not in squeue yet
    # pending: in squeue
    # running: status "R" in squeue
    # finished: not in squeue anymore
    # aborting: user executed "hermes kill", still in squeue
    # aborted: not in squeue anymore
    # error: not in squeue anymore, scontrol returns job state FAILED

    scheduling = 0
    pending = 1
    running = 2
    finished = 3
    aborting = 4
    aborted = 5
    error = 6


Experiment = collections.namedtuple(
    'Experiment',
    [
        'name',          # str; unique, console-friendly;
                         # TODO: what should this be?
                         # maybe dir name on the cluster?
        'tags',          # [str]; Neptune tags
        'sweep_name',    # str; matches Sweep.name
        'working_dir',   # str
        'log_path',      # str
        'status',        # Status
        'started_at',    # datetime.datetime or None; when the exp has started
        'job_id',        # str or None; id on SLURM
        'url',           # str or None; Neptune URL
    ],
    #(TO)TODO: I would recommend to not use defaults in namedtuples since it does not work in Python < 3.8
    # defaults=(
    #     Status.scheduling,  # status
    #     None,               # started_at
    #     None,               # job_id
    #     None,               # url
    # ),
)

Sweep = collections.namedtuple(
    'Sweep',
    [
        'name',          # str; unique, console-friendly;
                         # probably related to Experiment.name
        'note',
        'due_date',
        'neptune_org',
        'neptune_project_name',
        'neptune_tags',
        'group_by',
        'to_do',
        'description',   # str; human-friendly
        'cluster',       # str; identifier of the cluster
                         # TODO: what should this be?
                         # maybe just user@gateway.edu.pl?
        'created_at',    # datetime.datetime; when the user ran "hermes run"
        'style',
        'predecessors',  # [Edge]; sweeps that precede this one in the causal graph
    ],

    # defaults=(
    #     None,  # url
    #     (),    # predecessors
    # ),
)


Edge = collections.namedtuple(
    'Edge',
    [
        'sweep_name',  # str; matches Sweep.name
        'label',       # str; human-friendly
    ],
)
